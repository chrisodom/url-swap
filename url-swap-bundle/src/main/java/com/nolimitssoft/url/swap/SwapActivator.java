package com.nolimitssoft.url.swap;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Dictionary;
import java.util.Hashtable;

import org.ops4j.lang.NullArgumentException;
import org.ops4j.pax.swissbox.property.BundleContextPropertyResolver;
import org.ops4j.util.property.PropertyResolver;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.url.AbstractURLStreamHandlerService;
import org.osgi.service.url.URLConstants;
import org.osgi.service.url.URLStreamHandlerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SwapActivator implements BundleActivator {

	public final Logger logger = LoggerFactory.getLogger(SwapActivator.class);
	
	private ConnectionFactory connectionFactory;
	
	private Configuration configuration;
	
	private BundleContext bundleContext;
	
	private PropertyResolver propertyResolver;
	
	private ServiceRegistration<?> handlerReg;
	
	private ServiceRegistration<?> managedServiceReg;
	
	public SwapActivator() {
		init();
	}
	
	private void init() {
		connectionFactory = new ConnectionFactory();
	}
	
	@Override
	public void start(BundleContext bundleContext) throws Exception {
		NullArgumentException.validateNotNull(bundleContext,"Bundle context");
		this.bundleContext = bundleContext;
		registerManagedService();
		registerHandler();
	}

	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		NullArgumentException.validateNotNull(bundleContext,"Bundle context");
		if (null != handlerReg) {
			handlerReg.unregister();
		}
		if (null != managedServiceReg) {
			managedServiceReg.unregister();
		}
		bundleContext = null;
	}
	
	private void registerHandler() {
		final Dictionary<String,Object> props = new Hashtable<String,Object>();
		props.put(URLConstants.URL_HANDLER_PROTOCOL, Constants.PROTOCOLS);
		handlerReg = bundleContext.registerService(URLStreamHandlerService.class.getName(), new Handler(), props);
	}
	
	private void registerManagedService() {
		try {
			managedServiceReg = OptionalConfigAdminHelper.registerManagedService(bundleContext, Constants.PID, this);
		} catch (Throwable ignore) {
			setResolver(new BundleContextPropertyResolver(bundleContext));
			managedServiceReg = null;
		}
	}

	synchronized PropertyResolver getResolver() {
		return propertyResolver;
	}
	
	synchronized void setResolver(final PropertyResolver propertyResolver) {
		this.propertyResolver = propertyResolver;
		configuration = connectionFactory.createConfiguration(propertyResolver);
	}
	
	private class Handler extends AbstractURLStreamHandlerService {

		@Override
		public URLConnection openConnection(final URL u) throws IOException {
			return connectionFactory.createConnection(bundleContext, u, configuration);
		}
		
	}
}
