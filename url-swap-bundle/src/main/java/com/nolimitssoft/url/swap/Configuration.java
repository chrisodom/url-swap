package com.nolimitssoft.url.swap;

import org.ops4j.util.property.PropertyResolver;

public class Configuration {

	private PropertyResolver propertyResolver;
	
	public Configuration(PropertyResolver propertyResolver) {
		this.propertyResolver = propertyResolver;
	}
	
	public String getProperty(String key) {
		return propertyResolver.get(key);
	}
}
