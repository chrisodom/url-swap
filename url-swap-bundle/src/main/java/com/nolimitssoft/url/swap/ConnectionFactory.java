package com.nolimitssoft.url.swap;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.ops4j.util.property.PropertyResolver;
import org.osgi.framework.BundleContext;

import com.nolimitssoft.url.swap.internal.SwapConnection;

public class ConnectionFactory {

	public URLConnection createConnection(final BundleContext bundleContext, final URL url, final Configuration configuration ) throws MalformedURLException, IOException {
		URLConnection con = new SwapConnection(url, configuration);
		return con;
	}
	
	public Configuration createConfiguration(PropertyResolver propertyResolver) {
		Configuration config = new Configuration(propertyResolver);
		return config;
	}
}
