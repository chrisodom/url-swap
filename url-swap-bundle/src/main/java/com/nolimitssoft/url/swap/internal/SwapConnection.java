package com.nolimitssoft.url.swap.internal;

import java.io.IOException;

import org.ops4j.lang.NullArgumentException;

import com.nolimitssoft.url.swap.Configuration;
import com.nolimitssoft.url.swap.Constants;



public class SwapConnection extends java.net.URLConnection {

	private java.net.URL proxyURL;
	private Configuration configuration;
	
	private final char stripSlash = '/';
	private final String openParameter = "\\$\\{";
	private final String closeParameter = "}";
	private final char closeParameterChar = '}';
	private final String NULL = "null";
	
	public SwapConnection(java.net.URL u, Configuration configuration) throws IOException {
		super(u);
		NullArgumentException.validateNotNull(u, "URL cannot be null");
		NullArgumentException.validateNotNull(configuration, "Service configuration");
		this.configuration = configuration;
		init();
	}
	
	private void init() throws IOException {
		if (null == proxyURL) {
			
			String swapLocation = getURL().toExternalForm();
			String strippedLocation = swapLocation.substring(swapLocation.indexOf(Constants.PROTOCOL+":")+5);
			
			while (strippedLocation.charAt(0) == stripSlash) {
				strippedLocation = strippedLocation.substring(1);
			}
			
			StringBuilder proxyLocation = new StringBuilder();
			
			String[] locationSplitforParams = strippedLocation.split(openParameter);
			
			if (locationSplitforParams.length > 1) {
				for (String paramSegment : locationSplitforParams) {
					if (paramSegment.contains(closeParameter)) {
						String paramKey = paramSegment.substring(0,paramSegment.indexOf(closeParameterChar));
						String value = configuration.getProperty(paramKey);
						if (null == value || value.equals(NULL)) {
							if ( paramKey.equals(Constants.DEFAULT_ENV_KEY) ) {
								value = Constants.DEFAULT_ENV_VALUE;
							} else {
								throw new IOException("Error occurred while retreaving property for "+paramKey+", property not found. Please assign a value and try again.");
							}
						}
						proxyLocation.append(value).append(paramSegment.substring(paramSegment.indexOf(closeParameterChar)+1));
					} else {
						proxyLocation.append(paramSegment);
					}
				}
			} else {
				proxyLocation.append(strippedLocation);
			}
			
			proxyURL = new java.net.URL(proxyLocation.toString());
		}
	}

	@Override
	public void connect() throws IOException {
		// do nothing
	}
	
	
	@Override
	public java.io.InputStream getInputStream() throws java.io.IOException {
		return proxyURL.openStream();
	}

}
