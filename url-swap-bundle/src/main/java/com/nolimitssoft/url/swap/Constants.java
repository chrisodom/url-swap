package com.nolimitssoft.url.swap;

public interface Constants {

	public String[] PROTOCOLS = new String[]{"swap"};
	
	public String PID = "com.nolimitssoft.swap";
	
	public String DEFAULT_ENV_KEY = "deploy.env";
	
	public String DEFAULT_ENV_VALUE = "DEV";
	
	public String PROTOCOL = "swap";
}
