url-swap
==================

URL - Swap - Project

#Swap Protocol

The custom URL Swap is an OSGi URL handler that can proxy URL's and swap out parameters for system variables.


#Why

The swap protocol was developed to support the ability to proxy URL's that may need, for instance, environment specific information. For example, when retrieving configuration files during a karaf feature install there may be the need to have environment specific versions of the configuration file deployed. With the swap protocol you can use a predefined system variable to replace parameters with in the suffixed mvn URL 


#Overview

By using the swap protocol you can swap out defined parameters with in another URL. The swapping process can perform two distinct operations:

    1. swap out the ${deploy.env} parameter with either DEV, QA, STG or PROD, defaults to DEV.
    2. swap out any ${...} parameter with a system defined variable, throws IOException if not defined.

The first operation being the main reason the swap protocol was developed.


#Syntax

    swap-uri := “swap:” swap-uri [ “$” swap-parameter ]


#Configuration

The swap protocol is a karaf features based project that can either be installed via a features installation or embedded with in the containers system repository. If the artifact are available with in a remote repository the following commands can be used with in the container console to install the feature:

    JBossFuse:admin@root> features:addurl mvn:com.nolimitssoft.url.swap/url-swap-features/1.0.0/xml/features
    JBossFuse:admin@root> features:install swap

NOTE: The how to configure for the system repo information needs to added


#Examples

Swap a features configuration mvn URL for an environment based version. In this example it is assumed that the configuration being deployed was installed with the parameter being part of the mvn classifier.

    swap:mvn:com.example.deploy/proj-example/0.0.1-SNAPSHOT/cfg/proj-${deploy.env}

When this protocol is used and the environment variable is either set or not set to DEV the resulting mvn URL will look like the following:

    mvn:com.example.deploy/proj-example/0.0.1-SNAPSHOT/cfg/proj-DEV

NOTE: The default value for deploy.env is DEV.